import kivy
kivy.require("1.7.0")

from kivy.support import install_twisted_reactor
install_twisted_reactor()
from twisted.internet import reactor
from kivy.app import App
from kivy.uix.image import Image
from kivy.vector import Vector
from kivy.metrics import cm

from client import Factory


class Desktop(Image):

    touches = {}
    connection = None
    start = None
    end = None

    def on_touch_down(self, touch):
        self.touches[touch.uid] = touch
        if len(self.touches) == 2:
            t1 = self.touches.values()[0]
            t2 = self.touches.values()[1]
            self.start = Vector(t1.pos).distance(t2.pos)

    def on_touch_move(self, touch):
        if len(self.touches) == 2:
            t1 = self.touches.values()[0]
            t2 = self.touches.values()[1]
            self.end = Vector(t1.pos).distance(t2.pos)

    def on_connection(self, connection):
        print "connected"
        self.connection = connection

    def on_touch_up(self, touch):
        if self.touches.has_key(touch.uid):
            if len(self.touches) == 2 and self.start and self.end:
                print "UP:"
                print "\tend - start: %s" % cm(int(self.end - self.start))
                print "\tstart - end: %s" % cm(int(self.start - self.end))
                if self.connection:
                    if self.end - self.start > cm(1):
                        self.connection.sendLine("show_overview")
                    elif self.end - self.start < cm(1):
                        self.connection.sendLine("hide_overview")
            del self.touches[touch.uid]


class RemoteControlApp(App):

    def build(self):
        reactor.connectTCP('localhost', 8000, Factory(self.root.ids.img))

if __name__ == '__main__':
    RemoteControlApp().run()
