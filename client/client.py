from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineOnlyReceiver


class Client(LineOnlyReceiver):

    def connectionMade(self):
        self.factory.app.on_connection(self)


class Factory(ClientFactory):
    protocol = Client

    def __init__(self, app):
        self.app = app
