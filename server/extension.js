const Lang = imports.lang;

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Main = imports.ui.main;

const Interface =
    <interface name="de.maxludwig.interface">
        <method name="toggle_overview" />
        <method name="show_overview" />
        <method name="hide_overview" />
    </interface>;

// extension class
const AndroidRemoteControlExtension = new Lang.Class({
    Name: "AndroidRemoteControlExtension",

    _init: function() {
        this.impl = null;
        this._bus_id = null;
        this._prop = 'undef';
        this._propReadWrite = 'test';
    },

    enable: function() {
        this._bus_id = Gio.DBus.session.own_name('de.maxludwig.name',
                                                 Gio.BusNameOwnerFlags.NONE,
                                                 Lang.bind(this, this._on_name_acquired),
                                                 Lang.bind(this, this._on_name_lost));
    },

    disable: function() {
        Gio.DBus.session.unown_name(this._busWatchId);
        this._bus_id = null;
    },

    toggle_overview: function() {
        Main.overview.toggle();
    },

    show_overview: function() {
        Main.overview.show();
    },

    hide_overview: function() {
        Main.overview.hide();
    },

    _on_name_acquired: function(connection, name, nameOwner) {
        this.impl = Gio.DBusExportedObject.wrapJSObject(Interface, this);
        this.impl.export(Gio.DBus.session, '/de/maxludwig/object');
    },

    _on_name_lost: function(connection, name) {}
});

function init() {
    return new AndroidRemoteControlExtension();
}

