#!/usr/bin/gjs

const Gio= imports.gi.Gio;
const GLib = imports.gi.GLib;
const Mainloop = imports.mainloop;

const Lang = imports.lang;

const Iface1 = <interface name="de.maxludwig.iface1">
                   <method name="getcolor">
                       <arg type="i" direction="in" name="number" />
                       <arg type="s" direction="out" name="color" />
                   </method>
                   <property name="Prop1" type="s" access="readwrite" />
                   <property name="PropReadWrite" type="v" access="readwrite" />
               </interface>;

const DBusServer = new Lang.Class({
    Name: 'DBusServer',
    _init: function() {
        this.impl = null;
        this._name1_id = null;
        this._prop1 = 'undef';
        this._propReadWrite = 'test';
/*
        const BUS_NAME = "de.maxludwig.name1";
        const OBJECT1 = '/de/maxludwig/object1';
*/
    },
    run: function() {
        this._name1_id = Gio.DBus.session.own_name('de.maxludwig.name1',
                                                   Gio.BusNameOwnerFlags.NONE,
                                                   Lang.bind(this, this._on_name_acquired),
                                                   Lang.bind(this, this._on_name_lost));
        Mainloop.run('runMainloop');
    },
    stop: function() {
        Gio.DBus.session.unown_name(this._name1_id);
    },
    getcolor: function(number) {
        print('number is :',number);
        return "blue";
    },
    set Prop1(name) {
        this._prop1 = name;
        print('set Prop1 : ',name);
    },
    get Prop1() {
        print('get Prop1 : ',this._prop1);
        return this._prop1;
    },
    get PropReadWrite() {
        return new GLib.Variant('s', this._propReadWrite.toString());
    },
    _on_name_acquired: function(name) {
        print('Acquired name de.maxludwig.name1');
        this.impl = Gio.DBusExportedObject.wrapJSObject(Iface1, this);
        this.impl.export(Gio.DBus.session, '/de/maxludwig/object1');
    },
    _on_name_lost: function(name) {
        print('Lost name de.maxludwig.name1');
        Mainloop.quit('runMainloop');
    }
});

var server = new DBusServer();
server.run();
