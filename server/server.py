from __future__ import print_function
from twisted.internet import protocol, reactor
from twisted.protocols.basic import LineOnlyReceiver
import dbus


class Server(LineOnlyReceiver):
    nothing = lambda: print("no dbus available")
    command_not_found = lambda: print("command not found")
    api = {
        "toggle_overview": nothing,
        "show_overview": nothing,
        "hide_overview": nothing
    }

    def connectionMade(self):
        bus = dbus.SessionBus()
        service = bus.get_object('de.maxludwig.name', '/de/maxludwig/object')
        for key in self.api:
            self.api[key] = service.get_dbus_method(key, 'de.maxludwig.interface')
        print("connected")

    def lineReceived(self, data):
        self.api.get(data, self.command_not_found)()


class Factory(protocol.ServerFactory):
    protocol = Server

if __name__ == '__main__':
    reactor.listenTCP(8000, Factory())
    reactor.run()
