#!/usr/bin/gjs

const Gio= imports.gi.Gio;
const Mainloop = imports.mainloop;

const Lang = imports.lang;

const iface = <interface name="de.maxludwig.iface1">
                  <method name="getcolor">
                      <arg type="i" direction="in" name="number" />
                      <arg type="s" direction="out" name="color" />
                  </method>
                  <property name="Prop1" type="s" access="readwrite" />
                  <property name="PropReadWrite" type="v" access="readwrite" />
              </interface>;

const DBusClient = new Lang.Class({
    Name: 'DBusClient',
    _init: function() {
        this._prop1 = 'undef';
        this.ProxyClass = Gio.DBusProxy.makeProxyWrapper(iface);

        this.proxy = new this.ProxyClass(Gio.DBus.session,
                                         'de.maxludwig.name1',
                                         '/de/maxludwig/object1',
                                         Lang.bind(this, this._onError));
    },
    _onError: function(obj, error) {
        if (error) {
            print('error :', error);
        }
    },
    test: function() {
        var value = this.proxy.get_cached_property('Prop1');
        print('value prop1: ', value);
        this._prop1 = this.proxy.Prop1;
        print('type: ', typeof(this._prop1), ' Prop1: ', this._prop1);

        this.proxy.Prop1 = 'set';

        print('type: ', typeof(this.PropReadWrite), ' PropReadWrite: ', this.PropReadWrite);

        this.proxy.getcolorRemote(10, function(result, excp) {
            if (excp) {
                print('exception is :', excp);
            } else {
                [result] = result;
                print('type: ', typeof(result), ' resultat: ', result);
            }
            Mainloop.quit('runMainloop');
        });
    },
});

var client = new DBusClient();
client.test();
Mainloop.run('runMainloop');

/*
var [result,excp] = proxy.getcolorSync(10);
print('type: ',typeof(result),' resultat: ',result);
*/
